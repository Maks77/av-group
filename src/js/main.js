import $ from 'jquery';
import {TweenLite} from 'gsap';

$(document).ready(function () {
	$('.nav-menu-trigger').on('click', function () {
		$('.nav-menu').addClass('js-open')
		$(this).toggleClass('js-is-open')
		if ($(this).hasClass('js-is-open')) {
			TweenLite.fromTo($('.nav-menu'), .3, {x: 400}, {x: 0})
		} else {
			let tl = TweenLite.to($('.nav-menu'), .3, {x: 400});
			tl.eventCallback('onComplete', function () {
				$('.nav-menu').removeClass('js-open')
				this.totalProgress(0).pause();
			})
		}
	})
})